# bswc-observer

Discord bot to observe team's position in Cube Community's Tournaments

## Preview
![Message Example](https://i.imgur.com/bQfwCcw.png)

## Getting started

Clone `.env.example` as `.env` and fill in all the values:

| Key           	| Description                                                                                                                                                	|
|---------------	|------------------------------------------------------------------------------------------------------------------------------------------------------------	|
| DISCORD_TOKEN 	| Discord Bot's token                                                                                                                                        	|
| GUILD_ID      	| Target guild's id                                                                                                                                          	|
| CHANNEL_ID    	| Target channel's id                                                                                                                                        	|
| TOURNAMENT_ID 	| Cube Community's tournament id ("5" for BSWC 2022)                                                                                                         	|
| TEAM_NAME     	| Team's Display Name as listed on https://cube.community/main/tournament/bswc2022/qualifiers#team-leaderboards ("Russia",  "France", "United States", etc.) 	|

Run `yarn` to install the dependencies, `yarn build` to build the project.
Now you should be able to run the observer by `yarn start`.
