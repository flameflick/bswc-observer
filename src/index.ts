import 'dotenv/config';

import { observeLeaderboard } from './leaderboard-observer.js';

observeLeaderboard({
    discordToken: process.env.DISCORD_TOKEN!,
    guildId: process.env.GUILD_ID!,
    channelId: process.env.CHANNEL_ID!,
    tournamentId: process.env.TOURNAMENT_ID!,
    teamName: process.env.TEAM_NAME!
});