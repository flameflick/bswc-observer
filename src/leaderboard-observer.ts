import fetch from 'node-fetch';
import { schedule } from 'node-cron';

import { Client } from 'discordx';
import { Intents, TextChannel, MessageEmbed } from 'discord.js';

type ILeaderboardObserverConfig = {
    discordToken: string;
    guildId: string;
    channelId: string;
    tournamentId: string;
    teamName: string;
}

type ITournamentLeaderboardTeam = {
    team_id: string;
    name_id: string;
    display_name: string;

    pos: number;

    actual_acc: number;
    current_acc: number;
    scores_set: number;

    team_members: any[];
    captain: any;
}

const fetchCurrentPosition = async (tournamentId: string | number, teamName: string) => {
    const leaderboard = await (await fetch(`https://cube.community/api/tournament/get_leaderboard?tournID=${tournamentId}&team`)).json() as {
        team: ITournamentLeaderboardTeam[]
    };

    return leaderboard.team.findIndex(team => team.display_name === teamName) + 1;
};

export const observeLeaderboard = async (config: ILeaderboardObserverConfig) => {
    const bot = new Client({
        botGuilds: [config.guildId],

        intents: [
            Intents.FLAGS.GUILD_MESSAGES
        ]
    });

    bot.once('ready', async () => {
        await bot.guilds.fetch();

        const targetGuild = await bot.guilds.fetch(config.guildId);
        const targetChannel = await targetGuild.channels.fetch(config.channelId) as TextChannel;

        let previousPosition = await fetchCurrentPosition(config.tournamentId, config.teamName);
        
        schedule('*/5 * * * *', async () => {
            const currentPosition = await fetchCurrentPosition(config.tournamentId, config.teamName);
            const diff = currentPosition - previousPosition;

            if (diff === 0) return;

            const positionUpdate = new MessageEmbed()
                .setTitle(`${ config.teamName }'s leaderboard position was updated!`)
                .setDescription(`${ previousPosition } -> ${ currentPosition }`)
                .setColor(diff <= 0 ? 'GREEN' : 'RED')
                .setTimestamp()
                .setFooter({
                    text: 'https://gitlab.com/flameflick/bswc-observer'
                });
                
            targetChannel?.send({
                embeds: [ positionUpdate ]
            });

            previousPosition = currentPosition;
        });
    });

    bot.login(config.discordToken);
};